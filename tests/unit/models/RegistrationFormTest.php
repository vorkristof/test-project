<?php

namespace tests\unit\models;

use app\models\RegistrationForm;

class RegistrationFormTest extends \Codeception\Test\Unit
{
    private $model;

    public function testRegistrateUserCorrect()
    {
        $this->model = new RegistrationForm([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => '06301234567',
            'year_of_birth'     => '1993',
            'month_of_birth'    => 'január',
            'day_of_birth'      => '12',
            'email'             => 'test.user@test.com',
            'gdpr'              => true,
        ]);

        $this->model->validate();

        expect_that(empty($this->model->errors));
    }

    public function testRegistrateUserInvalidEmail()
    {
        $this->model = new RegistrationForm([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => '06301234567',
            'year_of_birth'     => '1993',
            'month_of_birth'    => 'január',
            'day_of_birth'      => '12',
            'email'             => 'invalid_email',
            'gdpr'              => true,
        ]);

        $this->model->validate();
        
        expect_that($this->model->errors['email']);
    }

    public function testRegistrateUserInvalidPhoneNumber()
    {
        $this->model = new RegistrationForm([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => 'invalid_phone_number',
            'year_of_birth'     => '1993',
            'month_of_birth'    => 'január',
            'day_of_birth'      => '12',
            'email'             => 'test.user@test.com',
            'gdpr'              => true,
        ]);

        $this->model->validate();

        expect_that($this->model->errors['phone_number']);
    }

    public function testRegistrateUserInvalidYearOfBirth()
    {
        $this->model = new RegistrationForm([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => '06301234567',
            'year_of_birth'     => '1400',
            'month_of_birth'    => 'január',
            'day_of_birth'      => '12',
            'email'             => 'test.user@test.com',
            'gdpr'              => true,
        ]);

        $this->model->validate();

        expect_that($this->model->errors['year_of_birth']);
    }

    public function testRegistrateUserInvalidMonthOfBirth()
    {
        $this->model = new RegistrationForm([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => '06301234567',
            'year_of_birth'     => '1993',
            'month_of_birth'    => 'invalid_month',
            'day_of_birth'      => '12',
            'email'             => 'test.user@test.com',
            'gdpr'              => true,
        ]);

        $this->model->validate();
        expect_that($this->model->errors['month_of_birth']);
    }

    public function testRegistrateUserInvalidDayOfBirth()
    {
        $this->model = new RegistrationForm([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => '06301234567',
            'year_of_birth'     => '1993',
            'month_of_birth'    => 'január',
            'day_of_birth'      => '100000',
            'email'             => 'test.user@test.com',
            'gdpr'              => true,
        ]);

        $this->model->validate();

        expect_that($this->model->errors['day_of_birth']);
    }

    public function testRegistrateUserRequired()
    {
        $this->model = new RegistrationForm([]);

        $this->model->validate();

        expect_that(strpos($this->model->errors['first_name'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['last_name'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['phone_number'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['year_of_birth'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['month_of_birth'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['day_of_birth'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['email'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['gdpr'][0], 'cannot be blank'));
    }
}
