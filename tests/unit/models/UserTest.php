<?php

namespace tests\unit\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    private $model;

    public function testUserCorrect()
    {
        $this->model = new User([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => '06301234567',
            'date_of_birth'     => date('Y-m-d', strtotime('1993-1-12')),
            'email'             => 'test.user@test.com',
            'gdpr'              => true,
        ]);

        $this->model->validate();
        
        $this->model->save();

        expect_that(empty($this->model->errors));
        expect_that(User::find()->count() > 0);
    }

    public function testUserInvalidEmail()
    {
        $this->model = new User([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => '06301234567',
            'date_of_birth'     => date('Y-m-d', strtotime('1993-1-12')),
            'email'             => 'invalid_email',
            'gdpr'              => true,
        ]);

        $this->model->validate();

        $this->model->save();
        var_dump(User::find()->count());
        die();
        expect_that($this->model->errors['email']);
        expect_that(User::find()->count() === 0);
    }

    public function testUserInvalidPhoneNumber()
    {
        $this->model = new User([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => 'invalid_phone_number',
            'date_of_birth'     => date('Y-m-d', strtotime('1993-1-12')),
            'email'             => 'test.user@test.com',
            'gdpr'              => true,
        ]);

        $this->model->validate();

        expect_that($this->model->errors['phone_number']);
    }

    public function testUserInvalidYearOfBirth()
    {
        $this->model = new User([
            'first_name'        => 'Test',
            'last_name'         => 'User',
            'phone_number'      => '06301234567',
            'date_of_birth'     => '1400',
            'email'             => 'test.user@test.com',
            'gdpr'              => true,
        ]);

        $this->model->validate();

        expect_that($this->model->errors['date_of_birth']);
    }

    public function testUserRequired()
    {
        $this->model = new User([]);

        $this->model->validate();

        expect_that(strpos($this->model->errors['first_name'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['last_name'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['phone_number'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['date_of_birth'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['email'][0], 'cannot be blank'));
        expect_that(strpos($this->model->errors['gdpr'][0], 'cannot be blank'));
    }
}
