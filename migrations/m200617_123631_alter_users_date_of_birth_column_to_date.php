<?php

use yii\db\Migration;

/**
 * Class m200617_123631_alter_users_date_of_birth_column_to_date
 */
class m200617_123631_alter_users_date_of_birth_column_to_date extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('users', 'date_of_birth', 'date');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('users', 'date_of_birth', 'string');
    }
}
