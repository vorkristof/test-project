<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * RegistrationForm is the model behind the registration form.
 */
class RegistrationForm extends Model
{
    public $first_name;
    public $last_name;
    public $phone_number;
    public $year_of_birth;
    public $month_of_birth;
    public $day_of_birth;
    public $email;
    public $gdpr;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // first name, last name, phone number, date of birth and email are required
            [['first_name', 'last_name', 'phone_number', 'year_of_birth', 'month_of_birth', 'day_of_birth', 'email', 'gdpr'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // phone number has to be valid
            ['phone_number', 'validatePhoneNumber'],
            // date of birth has to be a valid date address
            ['year_of_birth', 'integer', 'min' => 1900, 'max' => 2020],
            ['month_of_birth', 'validateMonth'],
            ['day_of_birth', 'integer', 'min' => 1, 'max' => 31],
        ];
    }

    /**
     * Validates the month.
     * This method serves as the inline validation for the month of the date.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateMonth($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!in_array($this->month_of_birth, ['január', 'február', 'március', 'április', 'május', 'június', 'július', 'augusztus', 'szeptember', 'oktober', 'november', 'december'])) {
                $this->addError($attribute, 'Nem megfelelő hónap.');
            }
        }
    }

     /**
     * Validates the phone number.
     * This method serves as the inline validation for the phone number.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePhoneNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (preg_match("/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$/", preg_quote($this->phone_number)) === 0) {
                $this->addError($attribute, 'Nem megfelelő telefonszám.');
            }
        }
    }

     /**
     * Give an array of valid birth dates.
     */
    public function getYearsList() 
    {
        $currentYear = date('Y');
        $yearFrom = 1900;
        $yearsRange = range($yearFrom, $currentYear);
        return array_combine($yearsRange, $yearsRange);
    }
}
