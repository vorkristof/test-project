<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * RegistrationForm is the model behind the registration form.
 */
class User extends ActiveRecord
{

    public static function tableName()
    {
        return '{{users}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // first name, last name, phone number, date of birth and email are required
            [['first_name', 'last_name', 'phone_number', 'date_of_birth', 'email', 'gdpr'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
             // phone number has to be valid
            ['phone_number', 'validatePhoneNumber'],
            // date of birth has to be a valid date
            ['date_of_birth', 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * Validates the phone number.
     * This method serves as the inline validation for the phone number.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePhoneNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (preg_match("/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$/", preg_quote($this->phone_number)) === 0) {
                $this->addError($attribute, 'Nem megfelelő telefonszám.');
            }
        }
    }
}
