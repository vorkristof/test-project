<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\RegistrationForm;
use app\models\User;
use DateTime;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Registration action.
     *
     * @return Response
     */
    public function actionRegistration()
    {
        $model = new RegistrationForm();

        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->getBodyParam('RegistrationForm');
            $date = $request['year_of_birth'] . '-' . $request['month_of_birth'] . '-' . $request['day_of_birth'];
            $user = new User();
            
            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->phone_number = $request['phone_number'];
            $user->date_of_birth = date('Y-m-d', strtotime($date));
            $user->email = $request['email'];
            $user->gdpr = $request['gdpr'];

            $user->save(false);

            $this->redirect('thanks');
        }
        
        return $this->render('registration', ['model' => $model]);
    }

    /**
     * Displays thanks page.
     *
     * @return string
     */
    public function actionThanks()
    {
        return $this->render('thanks');
    }

    /**
     * Displays thanks page.
     *
     * @return string
     */
    public function actionError()
    {
        $error = Yii::app()->errorHandler->error;
        if ($error) {
            $this->render('error', array('error'=>$error));
        } else {
            throw new CHttpException(404, 'Page not found.');
        }
    }
}
