-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
---------------------------------TEST PROJECT PROJECT---------------------------------
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------



INSTALLATION
------------

Create database:
- database name: test

Create admin:
- admin name: testadmin
- admin password: admin

Composer install:
- `composer install`

Run migrations:
- `php yii migrate`



RUN APPLICATION
---------------

Serve app:
- `php yii serve`
