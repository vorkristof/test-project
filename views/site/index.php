<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Test';
?>
<div class="site-index">
    <section>
        <div class="container">
            <h1 class="text-center">
                <a href="<?= Url::to('home'); ?>" title="OTP Bank" data-ua="3126984919">Test</a>
            </h1>
            <h2 class="text-center">
                <a class="btn btn-primary" href="<?= Url::to('registration') ?>">Szeretnék nyerni</a>            
            </h2>
        </div>
    </section>
</div>
