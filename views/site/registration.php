<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Test - Registration';
?>
<div class="site-registration">
    <h1 class="text-center" style="margin-bottom: 50px;">Először is add meg az adataidat!</h1>

    <?php $form = ActiveForm::begin([
        'id' => 'registration-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'wrapper' => 'col-sm-10',
            ],
        ],
    ]); ?>

        <div class="container">
            <div class="row" style="margin:auto;width:60%;">

                <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'last_name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'phone_number')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'year_of_birth')->dropdownList($model->getYearsList(), ['prompt'=>'Év']) ?>

                <?= $form->field($model, 'month_of_birth')->dropdownList([
                    '1' => 'január', 
                    '2' => 'február', 
                    '3' => 'március', 
                    '4' => 'április', 
                    '5' => 'május', 
                    '6' => 'június', 
                    '7' => 'július', 
                    '8' => 'augusztus', 
                    '9' => 'szeptember', 
                    '10' => 'oktober', 
                    '11' => 'november', 
                    '12' => 'december'
                ], ['prompt'=>'Hónap']) ?>
                
                <?= $form->field($model, 'day_of_birth')->dropdownList(range(1, 31), ['prompt'=>'Nap']) ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'gdpr')->checkbox([
                    'template' => "<div class=\"\">{input} Hozzájárulok a személyre szabott ajánlatok kidolgozása és ajánlattétel céljából történő adatkezeléshez</div>\n<div class=\"col-lg-8\">{error}</div>",
                ]) ?>

                <div class="form-group">
                    <div class="">
                        <?= Html::submitButton('Tovább', ['class' => 'btn btn-primary', 'name' => 'next-button']) ?>
                        <a href="<?= Url::to('home') ?>" class="btn btb-secondary">Vissza</a>
                    </div>
                </div>
        </div>

        </div>
    <?php ActiveForm::end(); ?>


</div>
