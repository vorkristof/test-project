<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Test - Thanks';
?>
<div class="site-about">
    <h1 class="text-center">Köszönjük, hogy részt vett a játékban!</h1>
    <h2 class="text-center">
        <a href="<?= Url::to('home') ?>">Vissza</a>
    </h2>
</div>
